all:	helloWorld

helloWorld:
	g++ helloWorld.cpp -o helloWorld
	chmod +x helloWorld

run:	helloWorld
	./helloWorld

clean:
	rm -f ./helloWorld
